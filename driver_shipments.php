<html>
<head>
    <link href="other_css/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="other_css/css/sb-admin.css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>



</head>
<body>

<?php
$connection = mysqli_connect('localhost', 'root', 'LP)mUJQc*,?4~@xd', 'shipper');
$vehicle_id = 1;
if(isset($_GET['submit'])){
    $data = $_GET['data'];
    $arr = explode(',', $data);
    $user_id = $arr[0];
    $vehicle_id = $arr[1];

    //var_dump($user_id);exit();

    $query = "SELECT * FROM shipment_details WHERE user_id = {$user_id}";

    $shipments = mysqli_query($connection, $query);
    if (!$shipments) {
        die("Query Failed" . mysqli_error($connection));
    }


}

$vehicle_status_query = "SELECT * FROM vehicle_status WHERE vehicle_id = $vehicle_id";
$vehicle = mysqli_query($connection, $vehicle_status_query);
if (!$vehicle) {
    die("Query Failed" . mysqli_error($connection));
}
$vehicle = $vehicle->fetch_assoc();

echo '<br>'."<strong>Partitions : </strong>" . $vehicle['partitions'] . '<br>';
echo "<strong>Vehicle Type : </strong>" . $vehicle['vehicle_type'] . '<br>';
echo "<strong>Occupied Slots : </strong>" . $vehicle['occupied_slots'] . '<br>';
echo "<strong>Available Slots : </strong>" . $vehicle['available_slots'] . '<br>';
?>


<table class="table table-bordered">
    <thead>
    <tr>
        <th>Driver ID</th>
        <th>Shipment ID</th>
        <th>Driver Name</th>
        <th>Pick up Time</th>
        <th>Drop off Time</th>
        <th>Time in Hours</th>
        <th>Pick Address</th>
        <th>Drop Address</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(isset($shipments)) {
        foreach ($shipments as $shipment) {

            $driver_id = $shipment['user_id'];
            $query = "SELECT name from users WHERE id = $driver_id";
            $driver_name = mysqli_query($connection, $query);
            if (!$driver_name) {
                die("Query Failed" . mysqli_error($connection));
            }
            $driver_name = $driver_name->fetch_row();

            ?>
            <tr>
                <td><?php echo $shipment['user_id']; ?></td>
                <td><?php echo $shipment['shipment_id']; ?></td>
                <td><?php echo $driver_name[0]; ?> </td>
                <td><?php echo $shipment['pick_up_time']; ?></td>
                <td><?php echo $shipment['drop_off_time']; ?></td>
                <td><?php echo timeCalculation($shipment['pick_up_location_lat'], $shipment['pick_up_location_lon'], $shipment['drop_off_location_lat'], $shipment['drop_off_location_lon']); ?></td>
                <td><?php echo $shipment['pick_address']; ?></td>
                <td><?php echo $shipment['drop_address']; ?></td>
            </tr>
        <?php }
    }?>




    </tbody>
</table>
</body>
</html>

<?php

function timeCalculation($lat1, $lon1, $lat2, $lon2) {

    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" . $lat1 . "," . $lon1 . "&destinations=" . $lat2 . "," . $lon2 . "&key=AIzaSyDqv6V2WxkR-QpK51QSxUFtBKLyY_pQdSM";
    $data = json_decode(file_get_contents($url));
    //$dist = $data['rows'];
    //var_dump($data);
    $distance = $data->rows[0]->elements[0]->distance->text;
//    echo "<br>";
    $time = $data->rows[0]->elements[0]->duration->value / 3600;
    $data = array('distance' => $distance, 'time' => $time);
    return round($time);
}
?>
