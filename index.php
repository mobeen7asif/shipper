<html>
<head>
    <link href="other_css/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="other_css/css/sb-admin.css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>
    
    <style>
        *{
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        body{
            margin:0px;
        }
    </style>



</head>
<body>

<?php
$connection = mysqli_connect('localhost','root','LP)mUJQc*,?4~@xd','shipper');
if(!$connection){

    die("Db conection failed");
}
$query = "SELECT * FROM vehicle_types";
$result = mysqli_query($connection, $query);

$query1 = "SELECT * FROM commodoties";
$result1 = mysqli_query($connection, $query1);

$query2 = "SELECT * FROM shipment_details";
$result2 = mysqli_query($connection, $query2);

//$locations = ['Lahore' => '31.483673,74.1861193','Islamabad' => '33.6690555,72.8448909','Peshawar' => '34.003010,71.459367',
//        'Multan' => '30.197157,71.463006', 'Faisalabad' => '31.363070,72.987473','Gujranwala' => '32.140566,74.186259',
//    'Shaikhu Pura' => '31.670550,74.203339','Hunza' => '36.315424,74.650030','kharian' => '32.811232,73.862400',
//    'Hyderabad' => '25.381959, 68.345617','Karachi' => '24.871656,67.028951', 'Muzaffrabad' => '34.357231,73.460830',
//    'Naran' => '34.906979,73.649082'
//];

$locations = [
    'Houston' => '29.762884400000001,-95.383061499999997', 'El Paso' => '31.758719800000001,-106.4869314',
    'Austin' => '30.267153,-97.743060799999995',
    'Victoria' => '28.805267400000002,-97.003598199999999', 'Petersburg' => '37.227927899999997,-77.401926700000004',
    'Tacoma' => '47.252876800000003,-122.4442906',
    'Oak Hill' => '37.972333900000002,-81.148713499999999', 'Huntington' => '38.419249600000001,-82.445154000000002',
    'Riverton' => '43.024959199999998,-108.3801036',
    'St. Paul' => '44.944167,-93.086074999999994', 'Greenville' => '33.410116100000003,-91.061773500000001',
    'Washington' => '35.546551700000002,-77.052174199999996',
    'Las Vegas' => '36.114646,-115.172816','New York' => '40.714269100000003,-74.005972900000003',
    'Mexicali' => '32.513997,-115.450391','Pecos County' => '31.000751,-103.068799',
    'Washington County' => '37.198182,-113.747643', 'Sedgwick County' => '37.860333,-97.290123',
    'Franklin County,' => '38.593656, -95.264001',
    'West Plains' => '37.254737,-100.588960', 'Wichita' => '37.612457,-97.329431',
    'Arizona' => '32.679825,-114.305972','New Mexico' => '32.259854,-108.151646',
    'Lordsburg' => '32.347094,-108.706630','El Cajon' => '32.802623,-116.971229',
    'Genoa' => '39.274798,-103.499083','Kit Carson County' => '39.285928,-103.070006',
    'Colorado' => '39.272130,-103.583264', 'Co Rd U' => '39.287247,-102.940176',
//
//    //case 2
//    'Elbert County' => '39.413781,-103.908489' , 'Kid Carsen County' => '39.304965,-102.497591',
//
//    //case 3 , 4
//    'Yuma' => '40.122120,-102.720360',
//    'Nabraska' => '41.116600,-101.724331',
//    'Crowly County' => '38.215239,-103.758016',
//
////case 4
//    'Greek County'=> '41.328288,-102.137866',
//    'Adam County' => '40.589560,-98.410545'
    
////    test case 1
    'Green River'=> '41.555906,-109.516539',
    'Licon Hwy'=> '41.439615,-110.092911',
//
////    test case 2
    'Willow Island' => '40.879578,-100.071706',
    'Lexington' => '40.741741,-99.741016',
    
//    test case 4
    'Rapid City' => '44.104237,-103.217505',
    'White Lake' => '43.738012,-98.795324',
];

//var_dump($result->fetch_all());exit();

$array = array('1', '1.30', '2');


if (!$result) {
    die("Query Failed" . mysqli_error($connection));
}
?>
<form action="" method="post" onkeypress="return event.keyCode != 13;">
    <span>Enter vehicle type</span>

    <select name="vehicle_type" >
        <?php while($row = mysqli_fetch_assoc($result)){ ?>
            <option value="<?php echo $row['name'];?>" <?php if(isset($_POST['vehicle_type'])){echo 'selected="selected"'; }?>><?php echo $row['name'];?></option>
        <?php } ?>
    </select>

    <span>Select Commodity Type</span>
    <select name="commodity" >
        <?php while($row = mysqli_fetch_assoc($result1)){ ?>
            <option value="<?php echo $row['title'];?>" <?php if(isset($_POST['commodity'])){echo 'selected="selected"'; }?>><?php echo $row['title'];?></option>
<!--            <option value="--><?php //echo $key;?><!--" --><?php //echo ($key ==  '2') ? ' selected="selected"' : '';?><!-->--><?php //echo $value;?><!--</option>-->
        <?php } ?>
    </select>

    <span>Select Pickup Location</span>
    <input id="pick_up_location" name="pick_address" type="text">

    <input name="pick_up_location_lat" class="PickLat" value="" type="text" hidden>
    <input name="pick_up_location_lon" class="PickLon" value="" type="text" hidden>

    <span>Select Drop off Location</span>
    <input id="drop_off_location" name="drop_address" type="text">

    <input name="drop_off_location_lat" class="DropLat" value="" type="text" hidden>
    <input name="drop_off_location_lon" class="DropLon" value="" type="text" hidden>

    <span>Enter Pickup Time</span>
    <input type="datetime" name="pick_up_time" value="<?php if(isset($_POST['pick_up_time'])){echo $_POST['pick_up_time'];}else{echo '';} ?>" class="datetimepicker">

    <span>Enter Drop off Time</span>
    <input type="datetime" name="drop_off_time" value="<?php if(isset($_POST['drop_off_time'])){echo $_POST['drop_off_time'];}else{echo '';} ?>" class="datetimepicker">

    <span>Enter Number Of Slots</span>
    <input type="number" name="slots" value="<?php if(isset($_POST['slots'])){echo $_POST['slots'];}else{echo 1;} ?>">



    <input type="submit" name="submit" value="Search">
</form>




<?php

$shipments = "SELECT * FROM shipments";

$shipments = mysqli_query($connection, $shipments);
if (!$shipments) {
    die("Query Failed" . mysqli_error($connection));
}
//$data = array();
//while($row = mysqli_fetch_assoc($shipments)){
//$user = "SELECT name FROM users where id = $user_id";
//$user = mysqli_query($connection, $fetch_users);
//$driver_name = $user->fetch_row();?>

<form method="get" action="driver_shipments.php">
<select name="data" >
    <?php while($row = mysqli_fetch_assoc($shipments)){
        $user_id = $row['user_id'];
        $user = "SELECT name FROM users where id = $user_id";
        $user = mysqli_query($connection, $user);
        $driver_name = $user->fetch_row();?>
        <option value="<?php echo $row['user_id'].','.$row['vehicle_id'];?>"><?php echo $driver_name[0]?></option>
    <?php } ?>
</select>
    <input type="submit" name="submit" value="Show Shipments">
</form>



<br>
<div style="float: left; width: 50%; padding-right: 10px;">
<label style="display: block;">Pick Up Location</label>
<div id="map_canvas" style="height: 350px;width: 100%;margin: 0.6em;"></div>
</div>
<div style="float: left; width: 50%; padding-left: 10px;">
<label style="display: block;">Drop Off Location</label>
<div id="drop_location" style="height: 350px;width: 100%;margin: 0.6em;"></div>
</div>
</body>

<script src="./jquery.js"></script>
<script src="build/jquery.datetimepicker.full.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCqc-BLADMwDPJa5RX03ZDGl4XYEB57ATs&libraries=places&region=uk&language=en&sensor=true"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>/*
     window.onerror = function(errorMsg) {
     $('#console').html($('#console').html()+'<br>'+errorMsg)
     }*/

    $.datetimepicker.setLocale('en');

    $('#datetimepicker_format').datetimepicker({value: '2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
    console.log($('#datetimepicker_format').datetimepicker('getValue'));

    $("#datetimepicker_format_change").on("click", function (e) {
        $("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
    });
    $("#datetimepicker_format_locale").on("change", function (e) {
        $.datetimepicker.setLocale($(e.currentTarget).val());
    });

    //asdasdad
    $('.datetimepicker').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: '0',
        formatTimeScroller: 'g:i A',
        format: 'Y-m-d H:i:s'
    });
    //$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

    $('.some_class').datetimepicker();

    $('#default_datetimepicker').datetimepicker({
        formatTime: 'H:i',
        formatDate: 'd.m.Y',
        //defaultDate:'8.12.1986', // it's my birthday
        defaultDate: '+03.01.1970', // it's my birthday
        defaultTime: '10:00',
        timepickerScrollbar: false
    });

    $('#datetimepicker10').datetimepicker({
        step: 5,
        inline: true
    });
    $('#datetimepicker_mask').datetimepicker({
        mask: '9999/19/39 29:59'
    });

    $('#datetimepicker1').datetimepicker({
        datepicker: false,
        format: 'H:i',
        step: 5
    });
    $('#datetimepicker2').datetimepicker({
        yearOffset: 222,
        lang: 'ch',
        timepicker: false,
        format: 'd/m/Y',
        formatDate: 'Y/m/d',
        minDate: '-1970/01/02', // yesterday is minimum date
        maxDate: '+1970/01/02' // and tommorow is maximum date calendar
    });
    $('#datetimepicker3').datetimepicker({
        inline: true
    });
    $('#datetimepicker4').datetimepicker();
    $('#open').click(function () {
        $('#datetimepicker4').datetimepicker('show');
    });
    $('#close').click(function () {
        $('#datetimepicker4').datetimepicker('hide');
    });
    $('#reset').click(function () {
        $('#datetimepicker4').datetimepicker('reset');
    });
    $('#datetimepicker5').datetimepicker({
        datepicker: false,
        allowTimes: ['12:00', '13:00', '15:00', '17:00', '17:05', '17:20', '19:00', '20:00'],
        step: 5
    });
    $('#datetimepicker6').datetimepicker();
    $('#destroy').click(function () {
        if ($('#datetimepicker6').data('xdsoft_datetimepicker')) {
            $('#datetimepicker6').datetimepicker('destroy');
            this.value = 'create';
        } else {
            $('#datetimepicker6').datetimepicker();
            this.value = 'destroy';
        }
    });
    var logic = function (currentDateTime) {
        if (currentDateTime && currentDateTime.getDay() == 6) {
            this.setOptions({
                minTime: '11:00'
            });
        } else
            this.setOptions({
                minTime: '8:00'
            });
    };
    $('#datetimepicker7').datetimepicker({
        onChangeDateTime: logic,
        onShow: logic
    });
    $('#datetimepicker8').datetimepicker({
        onGenerate: function (ct) {
            $(this).find('.xdsoft_date')
                .toggleClass('xdsoft_disabled');
        },
        minDate: '-1970/01/2',
        maxDate: '+1970/01/2',
        timepicker: false
    });
    $('#datetimepicker9').datetimepicker({
        onGenerate: function (ct) {
            $(this).find('.xdsoft_date.xdsoft_weekend')
                .addClass('xdsoft_disabled');
        },
        weekends: ['01.01.2014', '02.01.2014', '03.01.2014', '04.01.2014', '05.01.2014', '06.01.2014'],
        timepicker: false
    });
    var dateToDisable = new Date();
    dateToDisable.setDate(dateToDisable.getDate() + 2);
    $('#datetimepicker11').datetimepicker({
        beforeShowDay: function (date) {
            if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
                return [false, ""]
            }

            return [true, ""];
        }
    });
    $('#datetimepicker12').datetimepicker({
        beforeShowDay: function (date) {
            if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
                return [true, "custom-date-style"];
            }

            return [true, ""];
        }
    });
    $('#datetimepicker_dark').datetimepicker({theme: 'dark'})


</script>

<script>

    $(function () {
        var lat = 44.88623409320778,
            lng = -87.86480712897173,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
        var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_left
                }
            },
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
        var input = document.getElementById('pick_up_location');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.PickLat').val(place.geometry.location.lat());
            $('.PickLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event) {
            $('.PickLat').val(event.latLng.lat());
            $('.PickLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng
                    placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                    $("#pick_up_location").val(results[0].formatted_address);
                }
            });
        });
        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            //infowindow.open(map, marker);
        }
    });


    $(function () {
        var lat = 44.88623409320778,
            lng = -87.86480712897173,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
        var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_left
                }
            },
            map = new google.maps.Map(document.getElementById('drop_location'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
        var input = document.getElementById('drop_off_location');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.DropLat').val(place.geometry.location.lat());
            $('.DropLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event) {
            $('.DropLat').val(event.latLng.lat());
            $('.DropLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng
                    placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                    $("#drop_off_location").val(results[0].formatted_address);
                }
            });
        });
        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            //infowindow.open(map, marker);
        }
    });


</script>


</html>




<?php
if(isset($_POST['submit'])) {
//    echo '<pre>';
//    var_dump($_POST);
//    exit();
    $connection = mysqli_connect('localhost', 'root', 'LP)mUJQc*,?4~@xd', 'shipper');
$user_pick_up_time_input = '';
$user_drop_off_time_input = "";
$commodity = "";
$slots = 0;
$vehicle_type = "";
$lat1 = 0;
$lon1 = 0;
$lat2 = 0;
$lon2 = 0;
    $pick_address = '';
    $drop_address = '';
    if ($connection) {
        // Search Parameters from User
        $user_pick_up_time_input = $_POST['pick_up_time'];
        $user_drop_off_time_input = $_POST['drop_off_time'];

        $pick_address = $_POST['pick_address'];
        $drop_address = $_POST['drop_address'];

        $commodity = $_POST['commodity'];
        $slots = $_POST['slots'];
        $vehicle_type = $_POST['vehicle_type'];

        //$arr = explode(',', $_POST['pick_up_location']);
        $lat1 = $_POST['pick_up_location_lat'];
        $lon1 = $_POST['pick_up_location_lon'];

        //$arr = explode(',', $_POST['drop_off_location']);
        $lat2 = $_POST['drop_off_location_lat'];
        $lon2 = $_POST['drop_off_location_lon'];


        $user_input_shipment_time = round(abs(strtotime($user_drop_off_time_input) - strtotime($user_pick_up_time_input)) / 3600, 2);
        $google_time = timeCalculation($lat1, $lon1, $lat2, $lon2);
//        var_dump($user_input_shipment_time,$google_time);
//        exit();
        if ($user_input_shipment_time < $google_time) {
            echo "Your parcel cannot reach in this time";
            exit();
        }

        //Select Query which returns drivers
        $fetch_driver_query = "SELECT 
shipment_details.*,
  shipments.vehicle_id,
  vehicle_status.available_slots,
  shipment_vehicle_commodoty.title
FROM shipment_details
  LEFT JOIN shipments
    ON shipment_details.shipment_id = shipments.id
  LEFT JOIN vehicle_status
    ON shipments.vehicle_id = vehicle_status.vehicle_id
  LEFT JOIN shipment_vehicle_commodoty
    ON shipment_vehicle_commodoty.vehicle_id = shipments.vehicle_id
  AND shipment_vehicle_commodoty.shipment_id = shipment_details.shipment_id
    WHERE vehicle_status.available_slots >= {$slots} AND vehicle_status.vehicle_type = '{$vehicle_type}'
    AND shipment_vehicle_commodoty.title = '{$commodity}'
    GROUP BY shipment_details.shipment_id";

        $drivers = mysqli_query($connection, $fetch_driver_query);
        if (!$drivers) {
            die("Query Failed" . mysqli_error($connection));
        }

////        print_r($_POST);
//    print_r($drivers->fetch_all());
//    exit();
//    $d1 = new DateTime('2008-08-04 08:52:10');
//    $d2 = new DateTime('2008-08-03 11:11:10');
//
//    //var_dump($d1 == $d2);
//    var_dump($d1 > $d2);
//    //var_dump($d1 < $d2);


        $available_drivers = array();

        //check for the case
        $case = 0;
//        var_dump($drivers->fetch_all());
//        exit();
        while ($row = mysqli_fetch_assoc($drivers)) {

            $check = false;
            $check1 = false;
            $check2 = false;
            $check3 = false;
            $start_point = $row['pick_up_location_lat'] . ',' . $row['pick_up_location_lon'];
            $end_point = $row['drop_off_location_lat'] . ',' . $row['drop_off_location_lon'];

//            var_dump($start_point,$end_point);
//            exit();

            $check_point_lat = $lat1;
            $check_point_lng = $lon1;
//            echo $check_point_lat.'      '.$check_point_lng;
//            exit();
            $first_point = checkLocation($start_point, $end_point, $check_point_lat, $check_point_lng);
            //echo $check_point_lat."  ".$check_point_lng;
            //var_dump($first_point);
//            exit();
            $check_point_lat = $lat2;
            $check_point_lng = $lon2;
            
//            echo $lat1.'-----'.$lon1.'-----'.$lat2.'------'.$lon2;
//            exit();
            
//            echo $check_point_lat.'     '.$check_point_lng;
//            exit();
            $second_point = checkLocation($start_point, $end_point, $check_point_lat, $check_point_lng);

//            echo $lat1.' '.$lon1.' '.$lat2.' '.$lon2;
//            exit();

//            var_dump($first_point);
//            var_dump($second_point);
            //exit();

            if ($first_point and !$second_point) {
                $case = 1;
                //echo 'case 1'.'<br>';
                //die('case 1');
                //echo $row['shipment_id'].'<br>';
            }
            if ($first_point and $second_point) {
                $case = 2;
                //die('case 2');
                //echo 'case 2'.'<br>';
                //echo $row['shipment_id'].'<br>';
            }
            if (!$first_point and $second_point) {
                $case = 3;
                //die('case 3');
                //echo 'case 3'.'<br>';
                //echo $row['shipment_id'].'<br>';
            }
            if (!$first_point and !$second_point) {
                $case = 4;
                //die('case 4');
                //echo 'case 4'.'<br>';
                //echo $row['shipment_id'].'<br>';

            }

            if ($case == 1) {
                //first_shipment_time
                $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);


                //lahore to multan
                $time1 = ceil(timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $lat1, $lon1));


                //multan to islamabad
                $time2 = timeCalculation($lat1, $lon1, $row['drop_off_location_lat'], $row['drop_off_location_lon']);

                //islamabad to peshawar
                $time3 = timeCalculation($row['drop_off_location_lat'], $row['drop_off_location_lon'], $lat2, $lon2);

                //first time check
                $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));

                $driver_time_reach_time = new DateTime($new_time);
                $user_pick_up_time = new DateTime($user_pick_up_time_input);

//                var_dump($driver_time_reach_time,$user_pick_up_time);
//                exit();

                if ($driver_time_reach_time < $user_pick_up_time) {
                    $check1 = true;
                    if ($time1 + $time2 <= $first_shipment_time) {
                        $check2 = true;
                    }
                }
                $user_drop_off_time_input = strtotime($user_drop_off_time_input);
                $user_pick_up_time_input = strtotime($user_pick_up_time_input);
                $second_shipment_drop_off_time = round(abs($user_drop_off_time_input - $user_pick_up_time_input) / 3600, 2); //in hours

                if (($time2 + $time3) <= $second_shipment_drop_off_time) {
                    $check3 = true;
                }
                if ($check1 and $check2 and $check3) {
                    $available_drivers[] = $row;
                } else {

                }
            }

            if ($case == 2) { // when both start and drop location are on same route
                $check = false;
                $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);

                //lahore to multan
                $time1 = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $lat1, $lon1);
                //multan to faisalabad
                $time2 = timeCalculation($lat1, $lon1, $lat2, $lon2);

                //faisalabad to islamabad
                $time3 = timeCalculation($lat2, $lon2, $row['drop_off_location_lat'], $row['drop_off_location_lon']);

//                $date1= "2014-07-03 11:00:00";
//                $new_date= date("Y-m-d H:i:s", strtotime($date1 . " +3 hours"));
                //first time check
                $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));
                $driver_time_reach_time = new DateTime($new_time);
                $user_pick_up_time = new DateTime($user_pick_up_time_input);

                //$time_diff = round(abs($user_drop_off_time_input - $user_pick_up_time_input) / 3600, 2); //in hours
//                var_dump($driver_time_reach_time,$user_pick_up_time);
//                exit();
                if ($driver_time_reach_time < $user_pick_up_time) {
                    $check = true;
//                    var_dump($time1 + $time2 + $time3);
//                    exit();
//                    if (($time1 + $time2 + $time3) <= $first_shipment_time) {
//                        $check = true;
//                    }
                    if ($check) {
                        $available_drivers[] = $row;
                    } else {

                    }
                }
            }

            if ($case == 3) {
                $check = false;
                //Hyderabad to islamabad
                $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);
                //hyderabad to karachi
                $time1 = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $lat1, $lon1);
//                echo $row['pick_up_location_lat'],'----'.$row['pick_up_location_lon'].'----'.$lat1.'-----'.$lon1;
//                exit();
                //karachi to multan
                $time2 = timeCalculation($lat1, $lon1, $lat2, $lon2);

                //multan to islamabad
                $time3 = timeCalculation($lat2, $lon2, $row['drop_off_location_lat'], $row['drop_off_location_lon']);

//        $date1= "2014-07-03 11:00:00";
                $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));

                $driver_time_reach_time = new DateTime($new_time);
                $user_pick_up_time = new DateTime($user_pick_up_time_input);

                //first time check
                $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));


                $driver_time_reach_time = new DateTime($new_time);
                $user_pick_up_time = new DateTime($user_pick_up_time_input);

                //$time_diff = round(abs($user_drop_off_time_input - $user_pick_up_time_input) / 3600, 2); //in hours
//                var_dump($driver_time_reach_time,$user_pick_up_time);
//                exit();
                if ($driver_time_reach_time < $user_pick_up_time) {
                    if (($time2 + $time3) <= $first_shipment_time) {
                        $check = true;
                    }
                }
                if ($check) {
                    $available_drivers[] = $row;
                }
            }

            if ($case == 4) {

//                if($row['shipment_id'] == 2){
//                    die('its case 4 and shipment 2');
//                }
                //Lahore to Islamabad
                $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);

                //Lahore to Multan
                $time1 = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $lat1, $lon1);

                if($row['shipment_id'] == 26){

                    //var_dump($driver_time_reach_time,$user_pick_up_time);

                    var_dump($row['pick_up_location_lat'],$row['pick_up_location_lon'],$lat1,$lon1);
                    exit();
                }

                //lahore to faislabad
                $time2 = timeCalculation($lat1, $lon1, $lat2, $lon2);
                //Faislabad to Multan
                $time3 = timeCalculation($lat2, $lon2, $row['drop_off_location_lat'], $row['drop_off_location_lon']);



                $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));
                $driver_time_reach_time = new DateTime($new_time);
                $user_pick_up_time = new DateTime($user_pick_up_time_input);
                if ($driver_time_reach_time < $user_pick_up_time) {

                    if (($time1 + $first_shipment_time) < $first_shipment_time) {
                        $check = true;
                        $available_drivers[] = $row;
                    }
                }
                if (!$check) {
                    $time1 = timeCalculation($row['drop_off_location_lat'], $row['drop_off_location_lon'], $lat1, $lon1);
                    $new_time = date("Y-m-d H:i:s", strtotime($row['drop_off_time'] . " +" . $time1 . " hours"));
                    $driver_time_reach_time = new DateTime($new_time);
                    $user_pick_up_time = new DateTime($user_pick_up_time_input);
                    //var_dump($driver_time_reach_time,$user_pick_up_time);

                    if ($driver_time_reach_time < $user_pick_up_time) {

                        $check = true;
                        $available_drivers[] = $row;

                    }
                }
            }

            if ($case == 5) {
                //Sahiwal to Multan
                $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);

                $time_from_start = timeCalculation($lat1, $lon1, $row['pick_up_location_lat'], $row['pick_up_location_lon']);

                $time_from_drop = timeCalculation($lat1, $lon1, $row['drop_off_location_lat'], $row['drop_off_location_lon']);

                if ($time_from_start < $time_from_drop) {

                    $first_shipment_time = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $row['drop_off_location_lat'], $row['drop_off_location_lon']);
                    //sahiwal to lahore
                    $time1 = timeCalculation($row['pick_up_location_lat'], $row['pick_up_location_lon'], $lat1, $lon1);
                    //lahore to faislabad
                    $time2 = timeCalculation($lat1, $lon1, $lat2, $lon2);
                    //Faislabad to Multan
                    $time3 = timeCalculation($lat2, $lon2, $row['drop_off_location_lat'], $row['drop_off_location_lon']);
                    //first time check
                    $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));

                    $driver_time_reach_time = new DateTime($new_time);
                    $user_pick_up_time = new DateTime($user_pick_up_time_input);

                    if ($user_pick_up_time > ($driver_time_reach_time + 1) and $user_pick_up_time < ($driver_time_reach_time - 1)) {
                        if (($time2 + $time3) < $first_shipment_time) {
                            $check = true;
                        }
                    }
                } else {
                    $new_time = date("Y-m-d H:i:s", strtotime($row['pick_up_time'] . " +" . $time1 . " hours"));

                    $driver_time_reach_time = new DateTime($new_time);
                    $user_pick_up_time = new DateTime($user_pick_up_time_input);
                }
            }
        }

        //json_encode($available_drivers);
        //print_r(mysqli_fetch_array($result));
    }


    //$connection = mysqli_connect('localhost', 'root', 'LP)mUJQc*,?4~@xd', 'shipper');
    $query1 = "SELECT
  COUNT(shipment_details.id) AS details_id,
  shipments.user_id AS driver_id,
  GROUP_CONCAT(shipment_details.user_id) AS client_id,
  shipments.vehicle_id,
  shipments.id AS shipment_id
FROM shipments
  LEFT JOIN shipment_details
    ON ( shipments.id = shipment_details.shipment_id AND shipment_details.status = 0)
GROUP BY shipments.id";


    $users = mysqli_query($connection, $query1);
//echo '<pre>';
//var_dump();
//exit();
    if (!$users) {
        die("Query Failed" . mysqli_error($connection));
    }

    $new_array = [];
    while($row = mysqli_fetch_assoc($users)){
        if($row['details_id'] == 0){
            $new_array = ['vehicle_id' => $row['vehicle_id'], 'shipment_id' => $row['shipment_id'], 'user_id' => $row['driver_id']
            ];
            $available_drivers[] = $new_array;
        }
    }

//    foreach($available_drivers as $driver){
//        echo $driver['user_id'].'<br>';
//    }
//    echo '<pre>';
//    var_dump($available_drivers);
//    exit();


    ?>








<?php }
?>

<?php

function checkLocation($start_point, $end_point, $check_point_lat, $check_point_lng) {
    $locations = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?origin=$start_point&destination=$end_point&mode=driving&key=AIzaSyCbHsVYx9BJhBqE2VZO3bWd__YbsX-LjYQ");
    $locations = json_decode($locations);
    $point_index = 0;

    $start = explode(',', $start_point);
    $start_lat = $start[0];
    $start_lng = $start[1];

    $end = explode(',', $end_point);
    $end_lat = $end[0];
    $end_lng = $end[1];


    if (isset($locations->routes[0]->legs[0]->steps)) {
        $break_points = $locations->routes[0]->legs[0]->steps;
        foreach ($break_points as $break_point) {
            
            $point_index++;
            /* $compare_miles = 100;
              if($point_index <= 10 or $point_index >= (count($break_points ) - 10))
              $compare_miles = 60;
              if($point_index <= 5 or $point_index >= (count($break_points ) - 5))
              $compare_miles = 30; */

            $lat = $break_point->start_location->lat;
            $lng = $break_point->start_location->lng;
            
            //echo $lat.'+++'.$lng.'<br>';
            

            $theta_start = $lng - $start_lng;
            $dist_start = sin(deg2rad($lat)) * sin(deg2rad($start_lat)) + cos(deg2rad($lat)) * cos(deg2rad($start_lat)) * cos(deg2rad($theta_start));
            $dist_start = acos($dist_start);
            $dist_start = rad2deg($dist_start);
            $dist_start = $dist_start * 60 * 1.1515;

            $theta_end = $lng - $end_lng;
            $dist_end = sin(deg2rad($lat)) * sin(deg2rad($end_lat)) + cos(deg2rad($lat)) * cos(deg2rad($end_lat)) * cos(deg2rad($theta_end));
            $dist_end = acos($dist_end);
            $dist_end = rad2deg($dist_end);
            $dist_end = $dist_end * 60 * 1.1515;


            $compare_miles = 200;
            if ($dist_start < 200){
                //echo 'yes'.'<br>';
                $compare_miles = $dist_start;
            }

            if ($dist_end < 200){
                //echo 'no'.'<br>';
                $compare_miles = $dist_end;
            }



            $theta = $lng - $check_point_lng;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($check_point_lat)) + cos(deg2rad($lat)) * cos(deg2rad($check_point_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);


            $miles = $dist * 60 * 1.1515;
            //echo $miles.' <><> '.$compare_miles.'<br>';

            if ($miles <= $compare_miles) {
//                echo "<br>compare = " . $compare_miles;
//                echo "<br>start = " . $dist_start * 60 * 1.1515;
//                echo "<br>end = " . $dist_end * 60 * 1.1515;
//                echo "<br>miles = " . $miles * 60 * 1.1515;
//                echo "<br>true".$miles." ".$lat." ".$lng;
                return TRUE;
            }
        }
        return FALSE;
    } else {
        return FALSE;
    }
}

function timeCalculation($lat1, $lon1, $lat2, $lon2) {

    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" . $lat1 . "," . $lon1 . "&destinations=" . $lat2 . "," . $lon2 . "&key=AIzaSyDqv6V2WxkR-QpK51QSxUFtBKLyY_pQdSM";
    $data = json_decode(file_get_contents($url));
    //$dist = $data['rows'];
    //var_dump($data);
    $distance = $data->rows[0]->elements[0]->distance->text;
//    echo "<br>";
    $time = $data->rows[0]->elements[0]->duration->value / 3600;
    $data = array('distance' => $distance, 'time' => $time);
    return round($time);
}



$user_id = $_GET['id'];



//var_dump($available_drivers);
//exit();

?>



<?php if (isset($available_drivers)) {?>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Driver ID</th>
            <th>Shipment ID</th>
            <th>Driver Name</th>
            <th>Vehicle ID</th>
            <th>Pick Up Time</th>
            <th>Drop Off Time</th>
            <th>Pick Address</th>
            <th>Drop Address</th>
            <th>Book Now</th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($available_drivers as $driver) {
            $driver_id = $driver['user_id'];
        $query = "SELECT name from users WHERE id = $driver_id";
        $driver_name = mysqli_query($connection, $query);
        if (!$driver_name) {
            die("Query Failed" . mysqli_error($connection));
        }
        $driver_name = $driver_name->fetch_row();
$vehicle_id = $driver['vehicle_id'];
            ?>
        <tr>
                <td><?php echo $driver['user_id']; ?></td>
                <td><?php echo $driver['shipment_id']; ?></td>
                <td> <a href="detail.php?id=<?php echo $driver_id; ?>&vehicle_id=<?php echo $vehicle_id; ?>"> <?php echo $driver_name[0]; ?> </a> </td>
                <td><?php echo $driver['vehicle_id']; ?></td>
                <td><?php echo $driver['pick_up_time']; ?></td>
                <td><?php echo $driver['drop_off_time']; ?></td>
            <td><?php echo $driver['pick_address']; ?></td>
            <td><?php echo $driver['drop_address']; ?></td>
                <td>
                    <form action="booknow.php" method="post">
                        <input type="submit" name="submit" value="BOOK NOW"/>
                        <input type="hidden" name="user_pick_up_time_input"  value="<?php echo $user_pick_up_time_input; ?> ">
                        <input type="hidden" name="user_drop_off_time_input"  value="<?php echo $user_drop_off_time_input; ?> ">
                        <input type="hidden" name="commodity"  value="<?php echo $commodity; ?> ">
                        <input type="hidden" name="slots"  value="<?php echo $slots; ?> ">
                        <input type="hidden" name="vehicle_type"  value="<?php echo $vehicle_type; ?> ">
                        <input type="hidden" name="lat1"  value="<?php echo $lat1; ?> ">
                        <input type="hidden" name="lon1"  value="<?php echo $lon1; ?> ">
                        <input type="hidden" name="lat2"  value="<?php echo $lat2; ?> ">
                        <input type="hidden" name="lon2"  value="<?php echo $lon2; ?> ">
                        <input type="hidden" name="driver_id"  value="<?php echo $driver['user_id']; ?> ">
                        <input type="hidden" name="shipment_id"  value="<?php echo $driver['shipment_id']; ?> ">
                        <input type="hidden" name="driver_name"  value="<?php echo $driver_name[0]; ?> ">
                        <input type="hidden" name="vehicle_id"  value="<?php echo $driver['vehicle_id']; ?> ">

                        <input type="hidden" name="pick_address"  value="<?php echo $pick_address; ?> ">
                        <input type="hidden" name="drop_address"  value="<?php echo $drop_address; ?> ">
                    </form>
                </td>
        </tr>
            <?php } ?>




        </tbody>
    </table>

<?php } ?>

</body>
</html>


